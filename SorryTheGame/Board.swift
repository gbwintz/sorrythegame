//
//  Board.swift
//
//  SorryTheGame
//
//  Created by Gabrielle Winterton on 5/3/16.
//  Copyright (c) 2016 Gabrielle Winterton. All rights reserved.
//

import Foundation
import SpriteKit

class Board {
    var baordSize:Int = 59
   
    var boardMatrix = Array<BoardSpot>()
    var playesTurn:String = "UserName"
    init(){
        
            var colorArray = Array<String>()
            colorArray.append("Red")
            colorArray.append("Blue")
            colorArray.append("Yellow")
            colorArray.append("Green")

            for index in 0...baordSize {
                boardMatrix.append(BoardSpot(Index: index))
            }
            getBoardSpot(1).setSpeedUp(3)
            getBoardSpot(9).setSpeedUp(4)
            getBoardSpot(16).setSpeedUp(3)
            getBoardSpot(24).setSpeedUp(4)
            getBoardSpot(31).setSpeedUp(3)
            getBoardSpot(38).setSpeedUp(4)
            getBoardSpot(45).setSpeedUp(3)
            getBoardSpot(53).setSpeedUp(4)

        
    }
    
    func getBoardSpot(Index:Int)->BoardSpot{
        return boardMatrix[Index]
    }
    func setBoardPiece(Index:Int, piece:Piece){
        boardMatrix[Index].setPiece(piece)
    }
    
    func removeBoardPiece(index:Int){
        boardMatrix[index].removeSpot()
    }
    
    
}



/*
boardArray 0...60
starts at first red arrow
then, next arrow is +8, arrow after is +7, etc..
initialize each arrow with distance 3 for short and distance 4 for long

For each color (red,blue,yellow,green)


*/