//
//  StartScene.swift
//  SorryTheGame
//
//  Created by Gabrielle Winterton on 5/16/16.
//  Copyright © 2016 Gabrielle Winterton. All rights reserved.
//

import UIKit
import SpriteKit

class StartScene: SKScene {

    override func didMoveToView(view: SKView) {
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            let node = nodeAtPoint(location)
            
            if(node.name == "StartButton"){
                startGame()
            }
        }
        
    }

    func startGame(){
        
        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            let skView = self.view! as SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
        }
    }
    
    
}
