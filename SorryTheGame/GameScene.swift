//
//  GameScene.swift
//  SorryTheGame
//
//  Created by Gabrielle Winterton on 5/3/16.
//  Copyright (c) 2016 Gabrielle Winterton. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    var myBoard = Board()
    var redBoard = PieceBoard(color: "Red")
    var blueBoard = PieceBoard(color: "Blue")
    var yellowBoard = PieceBoard(color: "Yellow")
    var greenBoard = PieceBoard(color: "Green")
    var redSpriteArray = Array<SKSpriteNode>()
    var redCounter = 0
    var blueSpriteArray = Array<SKSpriteNode>()
    var blueCounter = 0
    var yellowSpriteArray = Array<SKSpriteNode>()
    var yellowCounter = 0
    var greenSpriteArray = Array<SKSpriteNode>()
    var greenCounter = 0
    var interfaceBoard = Array<SKSpriteNode>()
    var cardsArray = Array<SKSpriteNode>()
    var instructionsArray = Array<SKSpriteNode>()
    var cardDrawn:Bool = false
    let totalcards:UInt32 = 10
    
    var cardShown = SKSpriteNode()
    var backgroundShown = SKSpriteNode()
    var instructionShown = SKSpriteNode()
    
    var movementAray: Array<Int> = [1,2,3,4,5,7,8,10,11,12]
    var movement = 0
    var currentColor = "Red"
    var colorCounter = 0
    var pieceSelected = false
    var currentIndex = -1
    var highLightedMoves = Array<SKSpriteNode>()
    
    override func didMoveToView(view: SKView) {
        
        setUpUserBoard()
        
        //creates red start pieces
        var rednode = childNodeWithName("Red1") as! SKSpriteNode
        redSpriteArray.append(rednode)
        rednode = childNodeWithName("Red2") as! SKSpriteNode
        redSpriteArray.append(rednode)
        rednode = childNodeWithName("Red3") as! SKSpriteNode
        redSpriteArray.append(rednode)
        rednode = childNodeWithName("Red4") as! SKSpriteNode
        redSpriteArray.append(rednode)
        
        //creates blue start pieces
        self.enumerateChildNodesWithName("//Blue[0-9]*") { (node, stop) -> Void in
            let mynode = node as! SKSpriteNode
            self.blueSpriteArray.append(mynode)
            
        }
        
        //creates yellow start pieces
        self.enumerateChildNodesWithName("//Yellow[0-9]*") { (node, stop) -> Void in
            let mynode = node as! SKSpriteNode
            self.yellowSpriteArray.append(mynode)
        }
        
        //creates green start pieces
        self.enumerateChildNodesWithName("//Green[0-9]*") { (node, stop) -> Void in
            let mynode = node as! SKSpriteNode
            self.greenSpriteArray.append(mynode)
        }
        
        cardShown = childNodeWithName("Card") as! SKSpriteNode
        backgroundShown = childNodeWithName("Background") as! SKSpriteNode
        instructionShown = childNodeWithName("Instruction") as! SKSpriteNode
        for value in 1...12{
            if(value != 6){
                if(value != 9){
                    var node = SKSpriteNode(imageNamed: "Card\(value)")
                    cardsArray.append(node)
                    node = SKSpriteNode(imageNamed: "Instructions\(value)")
                    instructionsArray.append(node)
                }
                
            }
        }
        
        //get rid of sorry card, making it the card back
        //var node = SKSpriteNode(imageNamed: "CardBack")
        //cardsArray.append(node)
        //node = SKSpriteNode(imageNamed: "CardBack")
        //instructionsArray.append(node)
        
        
        print("Red Turn, Draw Card")
        backgroundShown.texture = SKTexture(imageNamed: "SorryGameBoardRed")
        
    }
    func drawCard(){
        
        let cardIndex = Int(arc4random_uniform(totalcards))
        
        cardShown.texture = cardsArray[cardIndex].texture
        instructionShown.texture = instructionsArray[cardIndex].texture
        instructionShown.zPosition = 2
        movement = movementAray[cardIndex]
    }
    func discard(){ //having some invalid requests while running but still works
        let node = SKSpriteNode(imageNamed: "CardBack")
        cardShown.texture = node.texture
        instructionShown.zPosition = -1
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            if(!cardDrawn){
                drawCard()
                cardDrawn = true
            }else {
                var moved = false
                //movement = 1 //comment out
                switch(currentColor){
                case "Red":
                    if(redBoard.getPiecesOnBoard() == 0){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            let red1 = Piece(name: "Red")
                            if(!checkForEnemy(getMatrixIndex(0))){
                                myBoard.setBoardPiece(getMatrixIndex(0), piece: red1)
                                
                            }
                            redBoard.setBoardPiece(0, piece: red1) //change back to zero
                            // //change to 4
                            //print(myBoard.getBoardSpot(19).getPiece().getColor()) //test
                            
                            updateBoard(0) //change back to zero
                            redBoard.incPiecesOnBoard()
                            redSpriteArray[redCounter].alpha = 0.0
                            redCounter += 1
                            moved =  true
                            nextTurn()
                        }else{
                            nextTurn()
                        }
                    }
                    break
                case "Blue":
                    if(blueBoard.getPiecesOnBoard() == 0){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            let blue1 = Piece(name: "Blue")
                            if(!checkForEnemy(getMatrixIndex(0))){
                                myBoard.setBoardPiece(getMatrixIndex(0), piece: blue1)
                            }
                            blueBoard.setBoardPiece(0, piece: blue1)//change to 0
                            // //change to 19
                            updateBoard(0)
                            blueBoard.incPiecesOnBoard()
                            blueSpriteArray[blueCounter].alpha = 0.0
                            blueCounter += 1
                            moved =  true
                            nextTurn()
                        }else{
                            nextTurn()
                        }
                    }
                    
                    break
                case "Yellow":
                    if(yellowBoard.getPiecesOnBoard() == 0){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            let yellow1 = Piece(name: "Yellow")
                            if(!checkForEnemy(getMatrixIndex(0))){
                                myBoard.setBoardPiece(getMatrixIndex(0), piece: yellow1)
                                
                            }
                            yellowBoard.setBoardPiece(0, piece: yellow1)
                            //myBoard.setBoardPiece(getMatrixIndex(0), piece: yellow1)
                            updateBoard(0)
                            yellowBoard.incPiecesOnBoard()
                            yellowSpriteArray[yellowCounter].alpha = 0.0
                            yellowCounter += 1
                            moved =  true
                            nextTurn()
                        }else{
                            nextTurn()
                        }
                    }
                    
                    break
                case "Green":
                    if(greenBoard.getPiecesOnBoard() == 0){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                          
                            let green1 = Piece(name: "Green")
                            if(!checkForEnemy(getMatrixIndex(0))){
                                
                                myBoard.setBoardPiece(getMatrixIndex(0), piece: green1)
                                
                            }
                            greenBoard.setBoardPiece(0, piece: green1)
                            //
                            updateBoard(0)
                            greenBoard.incPiecesOnBoard()
                            greenSpriteArray[greenCounter].alpha = 0.0
                            greenCounter += 1
                            moved =  true
                            nextTurn()
                        }else{
                            nextTurn()
                        }
                    }
                    
                    break
                default:break
                }
                
                if(!moved){
                    
                    
                    
                    
                    let location = touch.locationInNode(self)
                    
                    let node = nodeAtPoint(location)
                    
                    if let x = Int(node.name!){
                        
                        let newIndex = getOtherMatrixIndex(x, teamColor: currentColor)
                        
                        switch currentColor {
                        case "Red":
                            
                            let piece = redBoard.getBoardSpot(newIndex).getPiece()
                            let spot = redBoard.getBoardSpot(newIndex)
                            //switcharoo
                            print("red spot color\(spot.getColor())")
                            if(pieceSelected){
                                
                                pieceSelected = false
                                //check if clicked possible move
                                for move in highLightedMoves{
                                    if(node == move){
                                        piece.setColor("Red")
                                        movePiece(x, location: spot)
                                        
                                        
                                        piece.setCurrentIndex(getOtherMatrixIndex(x, teamColor: currentColor))
                                        clearHighLight()
                                    }
                                }
                                nextTurn()
                            }else
                                if(spot.getColor() == currentColor){
                                    
                                    clearHighLight()
                                    piece.setMovement(movement)
                                    
                                    getMoves(redBoard.getBoardSpot(newIndex))
                                    pieceSelected = true
                                    currentIndex = piece.getIndex()
                                    
                                }else{
                                    print("not a valid move")
                                    clearHighLight()
                                    
                            }
                            
                            break
                        case "Blue":
                            let piece = blueBoard.getBoardSpot(newIndex).getPiece()
                            let spot = blueBoard.getBoardSpot(newIndex)
                            //switcharoo
                            print("blue spot color\(spot.getColor())")
                            if(pieceSelected){
                                
                                pieceSelected = false
                                //check if clicked possible move
                                for move in highLightedMoves{
                                    if(node == move){
                                        piece.setColor("Blue")
                                        movePiece(x, location: spot)
                                        
                                        
                                        piece.setCurrentIndex(getOtherMatrixIndex(x, teamColor: currentColor))
                                        clearHighLight()
                                    }
                                }
                                nextTurn()
                            }else
                                if(spot.getColor() == currentColor){
                                    
                                    clearHighLight()
                                    piece.setMovement(movement)
                                    
                                    getMoves(blueBoard.getBoardSpot(newIndex))
                                    pieceSelected = true
                                    currentIndex = piece.getIndex()
                                    
                                }else{
                                    print("not a valid move")
                                    clearHighLight()
                                    
                            }
                            
                            
                            break
                        case "Yellow":
                            let piece = yellowBoard.getBoardSpot(newIndex).getPiece()
                            let spot = yellowBoard.getBoardSpot(newIndex)
                            //switcharoo
                            print("yellow spot color\(spot.getColor())")
                            if(pieceSelected){
                                
                                pieceSelected = false
                                //check if clicked possible move
                                for move in highLightedMoves{
                                    if(node == move){
                                        piece.setColor("Yellow")
                                        movePiece(x, location: spot)
                                        
                                        
                                        piece.setCurrentIndex(getOtherMatrixIndex(x, teamColor: currentColor))
                                        clearHighLight()
                                    }
                                }
                                nextTurn()
                            }else
                                if(spot.getColor() == currentColor){
                                    
                                    clearHighLight()
                                    piece.setMovement(movement)
                                    
                                    getMoves(yellowBoard.getBoardSpot(newIndex))
                                    pieceSelected = true
                                    currentIndex = piece.getIndex()
                                    
                                }else{
                                    print("not a valid move")
                                    clearHighLight()
                                    
                            }
                            
                            
                            break
                        case "Green":
                            let piece = greenBoard.getBoardSpot(newIndex).getPiece()
                            let spot = greenBoard.getBoardSpot(newIndex)
                            print("green spot color\(spot.getColor())")
                            //switcharoo
                            if(pieceSelected){
                                
                                pieceSelected = false
                                //check if clicked possible move
                                for move in highLightedMoves{
                                    if(node == move){
                                        piece.setColor("Green")
                                        movePiece(x, location: spot)
                                        
                                        
                                        piece.setCurrentIndex(getOtherMatrixIndex(x, teamColor: currentColor))
                                        clearHighLight()
                                    }
                                }
                                nextTurn()
                            }else
                                if(spot.getColor() == currentColor){
                                    
                                    clearHighLight()
                                    piece.setMovement(movement)
                                    
                                    getMoves(greenBoard.getBoardSpot(newIndex))
                                    pieceSelected = true
                                    currentIndex = piece.getIndex()
                                    
                                }else{
                                    print("not a valid move")
                                    clearHighLight()
                                    
                            }
                            
                            
                            break
                        default:
                            break
                        }
                        
                        
                    }else if(node.name == "\(currentColor)1"){ //for all the colors
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            switch(currentColor){
                            case "Red":
                                let red1 = Piece(name: "Red")
                                redBoard.setBoardPiece(0, piece: red1)
                                updateBoard(0)
                                redBoard.incPiecesOnBoard()
                                redSpriteArray[redCounter].alpha = 0.0
                                redCounter += 1
                                nextTurn()
                                break
                            case "Blue":
                                let blue1 = Piece(name: "Blue")
                                blueBoard.setBoardPiece(0, piece: blue1)
                                updateBoard(0)
                                blueBoard.incPiecesOnBoard()
                                blueSpriteArray[blueCounter].alpha = 0.0
                                blueCounter += 1
                                nextTurn()
                                break
                            case "Yellow":
                                let yellow1 = Piece(name: "Yellow")
                                yellowBoard.setBoardPiece(0, piece: yellow1)
                                updateBoard(0)
                                yellowBoard.incPiecesOnBoard()
                                yellowSpriteArray[yellowCounter].alpha = 0.0
                                yellowCounter += 1
                                nextTurn()
                                break
                            case "Green":
                                let green1 = Piece(name: "Green")
                                greenBoard.setBoardPiece(0, piece: green1)
                                updateBoard(0)
                                greenBoard.incPiecesOnBoard()
                                greenSpriteArray[greenCounter].alpha = 0.0
                                greenCounter += 1
                                nextTurn()
                                break
                            default:break
                                
                            }
                        }
                    }else if(node.name == "\(currentColor)2"){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            switch(currentColor){
                            case "Red":
                                let red2 = Piece(name: "Red")
                                redBoard.setBoardPiece(0, piece: red2)
                                updateBoard(0)
                                redBoard.incPiecesOnBoard()
                                redSpriteArray[redCounter].alpha = 0.0
                                redCounter += 1
                                nextTurn()
                                break
                            case "Blue":
                                let blue2 = Piece(name: "Blue")
                                blueBoard.setBoardPiece(0, piece: blue2)
                                updateBoard(0)
                                blueBoard.incPiecesOnBoard()
                                blueSpriteArray[blueCounter].alpha = 0.0
                                blueCounter += 1
                                nextTurn()
                                break
                            case "Yellow":
                                let yellow2 = Piece(name: "Yellow")
                                yellowBoard.setBoardPiece(0, piece: yellow2)
                                updateBoard(0)
                                yellowBoard.incPiecesOnBoard()
                                yellowSpriteArray[yellowCounter].alpha = 0.0
                                yellowCounter += 1
                                nextTurn()
                                break
                            case "Green":
                                let green2 = Piece(name: "Green")
                                greenBoard.setBoardPiece(0, piece: green2)
                                updateBoard(0)
                                greenBoard.incPiecesOnBoard()
                                greenSpriteArray[greenCounter].alpha = 0.0
                                greenCounter += 1
                                nextTurn()
                                break
                            default:break
                                
                            }
                        }
                    }else if(node.name == "\(currentColor)3"){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            switch(currentColor){
                            case "Red":
                                let red3 = Piece(name: "Red")
                                redBoard.setBoardPiece(0, piece: red3)
                                updateBoard(0)
                                redBoard.incPiecesOnBoard()
                                redSpriteArray[redCounter].alpha = 0.0
                                redCounter += 1
                                nextTurn()
                                break
                            case "Blue":
                                let blue3 = Piece(name: "Blue")
                                blueBoard.setBoardPiece(0, piece: blue3)
                                updateBoard(0)
                                blueBoard.incPiecesOnBoard()
                                blueSpriteArray[blueCounter].alpha = 0.0
                                blueCounter += 1
                                nextTurn()
                                break
                            case "Yellow":
                                let yellow3 = Piece(name: "Yellow")
                                yellowBoard.setBoardPiece(0, piece: yellow3)
                                updateBoard(0)
                                yellowBoard.incPiecesOnBoard()
                                yellowSpriteArray[yellowCounter].alpha = 0.0
                                yellowCounter += 1
                                nextTurn()
                                break
                            case "Green":
                                let green3 = Piece(name: "Green")
                                greenBoard.setBoardPiece(0, piece: green3)
                                updateBoard(0)
                                greenBoard.incPiecesOnBoard()
                                greenSpriteArray[greenCounter].alpha = 0.0
                                greenCounter += 1
                                nextTurn()
                                break
                            default:break
                                
                            }
                        }
                    }else if(node.name == "\(currentColor)4"){
                        if(movement == 1 || movement == 2){
                            //check if piece there
                            
                            switch(currentColor){
                            case "Red":
                                let red4 = Piece(name: "Red")
                                redBoard.setBoardPiece(0, piece: red4)
                                updateBoard(0)
                                redBoard.incPiecesOnBoard()
                                redSpriteArray[redCounter].alpha = 0.0
                                redCounter += 1
                                nextTurn()
                                break
                            case "Blue":
                                let blue4 = Piece(name: "Blue")
                                blueBoard.setBoardPiece(0, piece: blue4)
                                updateBoard(0)
                                blueBoard.incPiecesOnBoard()
                                blueSpriteArray[blueCounter].alpha = 0.0
                                blueCounter += 1
                                nextTurn()
                                break
                            case "Yellow":
                                let yellow4 = Piece(name: "Yellow")
                                yellowBoard.setBoardPiece(0, piece: yellow4)
                                updateBoard(0)
                                yellowBoard.incPiecesOnBoard()
                                yellowSpriteArray[yellowCounter].alpha = 0.0
                                yellowCounter += 1
                                nextTurn()
                                break
                            case "Green":
                                let green4 = Piece(name: "Green")
                                greenBoard.setBoardPiece(0, piece: green4)
                                updateBoard(0)
                                greenBoard.incPiecesOnBoard()
                                greenSpriteArray[greenCounter].alpha = 0.0
                                greenCounter += 1
                                nextTurn()
                                break
                            default:break
                                
                            }
                        }
                    }else{
                        print("select a piece")
                        clearHighLight()
                    }
                    
                    
                    
                }
            }
        }
    }
    func nextTurn(){
        cardDrawn = false
        discard()
        switch(currentColor){
        case "Red":
            currentColor = "Blue"
            backgroundShown.texture = SKTexture(imageNamed: "SorryGameBoardBlue")
            print("Blue Turn, Draw Card");
            break
        case "Blue":
            currentColor = "Yellow"
            backgroundShown.texture = SKTexture(imageNamed: "SorryGameBoardYellow")
            print("Yellow Turn, Draw Card")
            break
        case "Yellow":
            currentColor = "Green"
            backgroundShown.texture = SKTexture(imageNamed: "SorryGameBoardGreen")
            print("Green Turn, Draw Card")
            break
        case "Green":
            currentColor = "Red"
            backgroundShown.texture = SKTexture(imageNamed: "SorryGameBoardRed")
            print("Red Turn, Draw Card")
            break
        default:break
        }
        discard()
    }
    
    func getMoves(location:BoardSpot){ //need to make back into an array for 7 and 10 moves
        var moves: BoardSpot
        
        switch(currentColor){
            case "Red":
                moves = location.getPiece().showValidMoves(redBoard)
                highlightMoves(moves,location: location)
            break
            case "Blue":
                moves = location.getPiece().showValidMoves(blueBoard)
                highlightMoves(moves,location: location)
            break
            case "Yellow":
                moves = location.getPiece().showValidMoves(yellowBoard)
                highlightMoves(moves,location: location)
            break
            case "Green":
                moves = location.getPiece().showValidMoves(greenBoard)
                highlightMoves(moves,location: location)
            break
        default:break
        }
        
    }
    func highlightMoves(moves:BoardSpot,location:BoardSpot){
        
        
        let node = SKSpriteNode(imageNamed: "HighlightedBox")
        node.alpha = 1.00
        
        let temp = getMatrixIndex(moves.index)
        
        node.position = interfaceBoard[temp].position
        node.zPosition = 1
        node.name = "\(temp)"
        addChild(node)
        highLightedMoves.append(node)
        
    }
    func clearHighLight(){
        for move in highLightedMoves{
            move.removeFromParent()
        }
        highLightedMoves.removeAll()
    }
    //back and forth between the same two spaces for more than three consecutive turns is illegal try to put that in
    //x and y is new location
    func movePiece(index:Int, location:BoardSpot){
        //move piece to new location
        //check for battle

        //piece moved to new spot
        print("movePiece\(currentIndex)")
 
        print("index of color\(index)")
        let piece = location.getPiece()
        if(!checkForEnemy(index)){
            print("kaboom")
            print("remvoing\(getMatrixIndex(currentIndex))")
            print("setting \(index )")
            myBoard.setBoardPiece(index, piece: piece)
            myBoard.removeBoardPiece(getMatrixIndex(currentIndex))
            print("new piece set\(myBoard.getBoardSpot(index).getPiece().getColor())")
            print("new spot set\(myBoard.getBoardSpot(index).getColor())")
            print("old piece set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getPiece().getColor())")
            print("old spot set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getColor())")
        }
        moveSprite(index,  oldX: piece.getIndex(), imageName: piece.getname()) //prob wrong
        updateColor(index, oldX: piece.getIndex(), piece: piece)
        //
        
        
        //piece remove from old spot
        //location.removePiece()
        
        
        
    }
    func moveSprite(newX:Int,  oldX:Int, imageName:String){
        
        //userBoard[newX][newY].texture =  SKTexture(imageNamed: "RedCastle")
        //setImage(myBoard.getBoardSpot(newX).getPiece(), userPiece: userBoard[newX])
        print("sprite newX\(newX)")
        
        interfaceBoard[newX].texture =  SKTexture(imageNamed: "\(currentColor)Piece")
        interfaceBoard[newX].alpha = 1.0
        interfaceBoard[newX].name = "\(newX)"
        interfaceBoard[newX].size = CGSize(width: 15, height: 15)
        interfaceBoard[newX].zPosition = 1
        
        let temp = getOtherMatrixIndex(newX, teamColor: currentColor)
        
        //updateBoard(newX)
        
        let tempIndex = getMatrixIndex(currentIndex)
        print("sprite oldX\(tempIndex)")
        interfaceBoard[tempIndex].alpha = 0.0
        interfaceBoard[tempIndex].name = "sprite"
        interfaceBoard[tempIndex].zPosition = -1
        
        
        
    }
    func updateColor(newX:Int, oldX:Int,piece:Piece){
        
        //needs switch
        switch(currentColor){
            case "Red":
                print("color index remove\(currentIndex)")
                print("color index set\(getOtherMatrixIndex(newX%60, teamColor: currentColor))")
                redBoard.setBoardPiece(getOtherMatrixIndex(newX%60, teamColor: currentColor), piece: piece)
                redBoard.getBoardSpot(currentIndex).removeSpot()
                print("get old spot piece\(redBoard.getBoardSpot(currentIndex).getPiece().getColor())")
                print("get new spot piece\(redBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getPiece().getColor())")
                print("get old spot color\(redBoard.getBoardSpot(currentIndex).getColor())")
                print("get new spot color \(redBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getColor())")
                //redBoard.remove(currentIndex);
            break
            case "Blue":
                print("color index remove\(currentIndex)")
                print("color index set\(getOtherMatrixIndex(newX%60, teamColor: currentColor))")
                blueBoard.setBoardPiece(getOtherMatrixIndex(newX%60, teamColor: currentColor), piece: piece)
                blueBoard.getBoardSpot(currentIndex).removeSpot()
                print("get old spot piece\(blueBoard.getBoardSpot(currentIndex).getPiece().getColor())")
                print("get new spot piece\(blueBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getPiece().getColor())")
                print("get old spot color\(blueBoard.getBoardSpot(currentIndex).getColor())")
                print("get new spot color \(blueBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getColor())")
                //blueBoard.remove(currentIndex)
            break
            case "Yellow":
                print("color index remove\(currentIndex)")
                print("color index set\(getOtherMatrixIndex(newX%60, teamColor: currentColor))")
                yellowBoard.setBoardPiece(getOtherMatrixIndex(newX%60, teamColor: currentColor), piece: piece)
                yellowBoard.getBoardSpot(currentIndex).removeSpot()
                print("get old spot piece\(yellowBoard.getBoardSpot(currentIndex).getPiece().getColor())")
                print("get new spot piece\(yellowBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getPiece().getColor())")
                print("get old spot color\(yellowBoard.getBoardSpot(currentIndex).getColor())")
                print("get new spot color \(yellowBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getColor())")
                //yellowBoard.remove(currentIndex)
            break
            case "Green":
                print("color index remove\(currentIndex)")
                print("color index set\(getOtherMatrixIndex(newX%60, teamColor: currentColor))")
                greenBoard.setBoardPiece(getOtherMatrixIndex(newX%60, teamColor: currentColor), piece: piece)
                greenBoard.getBoardSpot(currentIndex).removeSpot()
                print("get old spot piece\(greenBoard.getBoardSpot(currentIndex).getPiece().getColor())")
                print("get new spot piece\(greenBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getPiece().getColor())")
                print("get old spot color\(greenBoard.getBoardSpot(currentIndex).getColor())")
                print("get new spot color \(greenBoard.getBoardSpot(getOtherMatrixIndex(newX%60, teamColor: currentColor)).getColor())")
                
                //greenBoard.remove(currentIndex)
            break
        default:break
        }

    }
    func checkForEnemy(x:Int)->Bool{
        //if enemy remove him from the game
        var result = false
        print("checking for enemy at\(x)")
        print(" enemy is there is \(!myBoard.getBoardSpot(x).isSpotEmpty()) statement")
        
        if(!myBoard.getBoardSpot(x).isSpotEmpty()){ //might be getMatrixIndex or whatever instead of x
            print("enemy ahoy!")
            result = true
            let enemy = myBoard.getBoardSpot(x)
            print(enemy.getColor())
            interfaceBoard[x].alpha = 0.0
            interfaceBoard[x].name = "sprite"
            interfaceBoard[x].zPosition = -1

            switch(enemy.getColor()){
                case "Red":
                    redBoard.decPiecesOnBoard()
                    redCounter -= 1
                    redSpriteArray[redCounter].alpha = 1.0
                    let p1 = myBoard.getBoardSpot(x) //get board piece
                    p1.removeSpot() //removes piece
                    print("remvoing board matix stuff\(getMatrixIndex(currentIndex))")
                    print("setting \(x) ")
                    myBoard.setBoardPiece(x, piece: p1.getPiece()) //puts removed piece back into board
                    myBoard.removeBoardPiece(getMatrixIndex(currentIndex))
                    print("new color in enemy piece set\(myBoard.getBoardSpot(x).getPiece().getColor())")
                    print("new color in enemy spot set\(myBoard.getBoardSpot(x).getColor())")
                    print("old color in enemy piece set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getPiece().getColor())")
                    print("old color in enemy spot set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getColor())")
                    
                break
                case "Blue":
                    blueBoard.decPiecesOnBoard()
                    blueCounter -= 1
                    blueSpriteArray[blueCounter].alpha = 1.0
                    let p1 = myBoard.getBoardSpot(x) //get board piece
                    p1.removeSpot() //removes piece
                    print("remvoing board matix stuff\(getMatrixIndex(currentIndex))")
                    print("setting \(x) ")
                    myBoard.setBoardPiece(x, piece: p1.getPiece()) //puts removed piece back into board
                    myBoard.removeBoardPiece(getMatrixIndex(currentIndex))
                    print("new color in enemy piece set\(myBoard.getBoardSpot(x).getPiece().getColor())")
                    print("new color in enemy spot set\(myBoard.getBoardSpot(x).getColor())")
                    print("old color in enemy piece set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getPiece().getColor())")
                    print("old color in enemy spot set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getColor())")
                break
                case "Yellow":
                    yellowBoard.decPiecesOnBoard()
                    yellowCounter -= 1
                    yellowSpriteArray[yellowCounter].alpha = 1.0
                    let p1 = myBoard.getBoardSpot(x) //get board piece
                    p1.removeSpot() //removes piece
                    print("remvoing board matix stuff\(getMatrixIndex(currentIndex))")
                    print("setting \(x) ")
                    myBoard.setBoardPiece(x, piece: p1.getPiece()) //puts removed piece back into board
                    myBoard.removeBoardPiece(getMatrixIndex(currentIndex))
                    print("new color in enemy piece set\(myBoard.getBoardSpot(x).getPiece().getColor())")
                    print("new color in enemy spot set\(myBoard.getBoardSpot(x).getColor())")
                    print("old color in enemy piece set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getPiece().getColor())")
                    print("old color in enemy spot set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getColor())")
                break
                case "Green":
                    greenBoard.decPiecesOnBoard()
                    greenCounter -= 1
                    greenSpriteArray[greenCounter].alpha = 1.0
                    let p1 = myBoard.getBoardSpot(x) //get board piece
                    p1.removeSpot() //removes piece
                    print("remvoing board matix stuff\(getMatrixIndex(currentIndex))")
                    print("setting \(x)")
                    myBoard.setBoardPiece(x, piece: p1.getPiece()) //puts removed piece back into board
                    myBoard.removeBoardPiece(getMatrixIndex(currentIndex))
                    print("new color in enemy piece set\(myBoard.getBoardSpot(x).getPiece().getColor())")
                    print("new color in enemy spot set\(myBoard.getBoardSpot(x).getColor())")
                    print("old color in enemy piece set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getPiece().getColor())")
                    print("old color in enemy spot set\(myBoard.getBoardSpot(getMatrixIndex(currentIndex)).getColor())")
                break
            default:
                break
            }
        }
        return result
        
    }
    
    func getMatrixIndex(spot:Int)->Int{
        var result = 0
        
        
        switch(currentColor){
        case "Red":
            result = 4
            break
        case "Blue":
            result = 19
            break
        case "Yellow":
            result = 34
            break
        case "Green":
            result = 49
            break
        default:break
            
        }
        result = spot + result
        if(result > 59){
            result = (result - 60)%60
        }
        return result
    }
    
    func getOtherMatrixIndex(index:Int, teamColor:String)->Int{
        var result = 0
        switch(teamColor){
        case "Red":
            result = 4
            break
        case "Blue":
            result = 19
            break
        case "Yellow":
            result = 34
            break
        case "Green":
            result = 49
            break
        default:break
            
        }
        result = index - result
        if(result < 0){
            result = (60 + result)%60
        }
        return result
    }
    
    
    
    func updateBoard(spot:Int){
        let newIndex = getMatrixIndex(spot)
        let newPiece = currentColor
        
        interfaceBoard[newIndex].texture = SKTexture(imageNamed: "\(newPiece)Piece")
        interfaceBoard[newIndex].alpha = 1.0
        interfaceBoard[newIndex].zPosition = 2
        interfaceBoard[newIndex].size = CGSize(width: 15, height: 15)
        interfaceBoard[newIndex].name = "\(newIndex)"
        
    }
    
    func setUpUserBoard(){
        
        self.enumerateChildNodesWithName("//spot[0-9]*") { (node, stop) -> Void in
            
            let spirte = SKSpriteNode(imageNamed: "HighlightedBox")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = 1
            spirte.name = "sprite"
            
            self.addChild(spirte)
            
            self.interfaceBoard.append(spirte)
        }
        
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}




/*

For each turn:

Write to screen "[Color]'s Turn, Tap to Draw Card"
Tap to draw card
highlight boxes that are valid moves
player will pick where to move
**maybe player can undo move before their turn is over
if move lands on another player, send the other player's piece to start.
Player will click end turn
Write to screen "Pass to next player" if more than one person is playing

*/