//
//  BoardSpot.swift
//
//  SorryTheGame
//
//  Created by Gabrielle Winterton on 5/3/16.
//  Copyright (c) 2016 Gabrielle Winterton. All rights reserved.
//

import Foundation
import SpriteKit

class BoardSpot{
    var index:Int = 0

    var myPiece:Piece
    var isEmpty:Bool = true
    var color:String = "White"
    var speedUp:Int = 0
    init(Index:Int){
        index = Index
        myPiece = Piece(name: "Blank")
        myPiece.setName("Blank")
        isEmpty = true
    }
    
    func setSpeedUp(distance:Int){
        speedUp = distance
    }
    func getSpeedUp()->Int{
        return speedUp
    }
    func getPiece()->Piece{
        return myPiece
    }
    func getColor()->String{
        return color
    }
    func setColor(newColor:String){
        color = newColor
    }
    func isSpotEmpty()->Bool{
        return isEmpty
    }
    func setPiece(piece:Piece){
        myPiece = piece
        piece.setCurrentIndex(index)
        isEmpty = false
        print("seeting the spot\(piece.getColor())")
        setColor(piece.getColor())
    }
    func removeSpot(){
        isEmpty = true
        //myPiece = Piece(name: "Blank")
        //myPiece.setName("Blank")
        setColor("Blank")
        
    }
    
}