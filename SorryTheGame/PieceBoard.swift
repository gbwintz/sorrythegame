//
//  Board.swift
//
//  SorryTheGame
//
//  Created by Gabrielle Winterton on 5/3/16.
//  Copyright (c) 2016 Gabrielle Winterton. All rights reserved.
//

import Foundation
import SpriteKit

class PieceBoard {
    var baordSize:Int = 65
   
    var boardMatrix = Array<BoardSpot>()
    var boardColor:String = "PlaceHolder"
    var startSpace:Int = 4
    var piecesOnBoard:Int = 0
    init(color:String){
            boardColor = color
            var colorArray = Array<String>()

            for index in 0...baordSize {
                boardMatrix.append(BoardSpot(Index: index))
            }
        
    }
    
    func getPiecesOnBoard()->Int{
        return piecesOnBoard
    }
    func incPiecesOnBoard(){
        piecesOnBoard += 1
    }
    func decPiecesOnBoard(){
        piecesOnBoard -= 1
    }
    func getBoardSpot(Index:Int)->BoardSpot{
            return boardMatrix[Index]
    }
    func setBoardPiece(Index:Int, piece:Piece){
        boardMatrix[Index].setPiece(piece)
    }
    func getStartSpace()->Int{
        return startSpace
    }
    func decStartSpace(){
        startSpace -= 1
    }
    func remove(Index:Int){
        boardMatrix[Index].removeSpot()
    }
    
    
}



/*
boardArray 0...60
starts at first red arrow
then, next arrow is +8, arrow after is +7, etc..
initialize each arrow with distance 3 for short and distance 4 for long

For each color (red,blue,yellow,green)


*/