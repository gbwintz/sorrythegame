//
//  Piece.swift
//
//  SorryTheGame
//
//  Created by Gabrielle Winterton on 5/3/16.
//  Copyright (c) 2016 Gabrielle Winterton. All rights reserved.
//

import Foundation
import SpriteKit

class Piece{
    //piece ablities
    var pieceMovement:Int = 0
    var pieceName:String = "name"
    
    var pieceDescription:String = "placeholder"
    
    
    
    var color:String = "White"
    
    
    var isHome:Bool = false
    var isStart:Bool = false
    var isActive:Bool = true
    var index:Int = 0
    init(name:String){
        
        color = name
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setMovement(value:Int){
        pieceMovement = value
    }
    func getMovement()->Int{
        return pieceMovement
    }
    
    func getname()->String{
        return pieceName
    }
    func setName(name:String){
        pieceName = name
    }
    func setColor(newColor:String){
        color = newColor
    }
    func getColor()->String{
        return color
    }
    
    func getIndex()->Int{
        return index
    }
    
    func setCurrentIndex(y:Int){
        index = y
    }
    
    func showValidMoves(board: PieceBoard)->BoardSpot{
        /*var validMoves: [BoardSpot] = []
        
        
        for location in 1...self.getMovement(){
        
        //check up/down left and right
        var tempIndex = index
        
        //check left
        
        tempIndex += location
        
        if(validMove(tempIndex, board: board)){
        
        validMoves.append(board.getBoardSpot(tempIndex))
        
        }
        
        
        }*/
        var validMoves:BoardSpot
        var tempIndex = index
        
        //check left
        if(getMovement() == 4){
            tempIndex -= getMovement()
            tempIndex += 60
        }
        else{
            tempIndex += getMovement()
        }
        tempIndex = tempIndex % 60
        
        validMoves = board.getBoardSpot(tempIndex)
        print(getMovement())
        print(index)
        print(tempIndex)
        print(validMoves.index)
        return validMoves
    }
    
    func validMove(x:Int,board:PieceBoard)->Bool{
        var result = true
        //check if on starthome spot
        if(board.getBoardSpot(x).getColor() == color){
            
        }else if(isStart && pieceMovement > 3){
            
        }else if(isHome && pieceMovement > distanceFromHome()){
            
        }
        
        return result
    }
    //true if enemy
    func distanceFromHome()->Int{
        var result = 0
        
        return result
    }
    func getIsActive()->Bool{
        return isActive
    }
    
    //might not need this
    //moght do movement in matrix and not in peices
    
    
    func move(X:Int){
        index = X
        
    }
    
}